<?php

namespace Drupal\boutique_carousel\Plugin\Block;

use Drupal\Component\Utility\Html;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;

/** @noinspection PhpUnused */

/**
 * @Block(
 *   id = "boutique_carousel_block",
 *   admin_label = @Translation("Boutique Carousel Block"),
 * )
 */
class BoutiqueCarouselBlock extends BlockBase {

  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $fids = $config['carousel_images'] ?? [];
    $carousel_images_config = $config['carousel_images_config'] ?? [];
    $form['prefix'] = [
      '#type' => 'text_format',
      '#default_value' => $config['prefix']['value'] ?? '',
      '#title' => new TranslatableMarkup('Prefix markup'),
      '#format' => $config['prefix']['format'] ?? '',
    ];
    $form['carousel_images'] = [
      '#type' => 'boutique_managed_file',
      '#title' => new TranslatableMarkup('Upload images'),
      '#description' => new TranslatableMarkup('Best to use portrait-style images'),
      '#required' => true,
      '#multiple' => true,
      '#upload_validators' => ['file_validate_extensions' => ['png jpg jpeg gif']],
      '#upload_location' => 'public://boutique-carousel-images/',
      '#default_value' => $fids ?? [],
      '#weight' => 10,
      '#carousel_images_config' => $carousel_images_config,
    ];
    $styles = \Drupal::entityTypeManager()->getStorage('image_style')->loadMultiple();
    $options = ['' => new TranslatableMarkup('No style')];
    foreach($styles as $name => $style) {
      $options[$name] = $style->label();
    }
    $form['style'] = [
      '#weight' => 55,
      '#title' => new TranslatableMarkup('Select image style to apply to images'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $config['style'] ?? '',
    ];
    $form['width'] = [
      '#weight' => 60,
      '#title' => new TranslatableMarkup('Image width (px)'),
      '#type' => 'textfield',
      '#default_value' => $config['width'] ?? 200,
    ];
    $form['height'] = [
      '#weight' => 65,
      '#title' => new TranslatableMarkup('Image height (px)'),
      '#type' => 'textfield',
      '#default_value' => $config['height'] ?? 400,
    ];
    $form['suffix'] = [
      '#type' => 'text_format',
      '#default_value' => $config['suffix']['value'] ?? '',
      '#title' => new TranslatableMarkup('Suffix markup'),
      '#format' => $config['suffix']['format'] ?? '',
    ];
    $form['autoplay'] = [
      '#type' => 'checkbox',
      '#weight' => 70,
      '#default_value' => $config['autoplay'] ?? false,
      '#title' => new TranslatableMarkup('Autoplay'),
      '#description' => new TranslatableMarkup('Automatically start to rotate images')
    ];
    $form['autoplay_interval'] = [
      '#type' => 'textfield',
      '#weight' => 75,
      '#default_value' => $config['autoplay_interval'] ?? 3000,
      '#title' => new TranslatableMarkup('Pictures rotation interval (milliseconds)'),
      '#description' => new TranslatableMarkup('How often the image will rotate one after the other'),
      '#states' => [
        'visible' => [':input[name="settings[autoplay]"]' => ['checked' => TRUE],],
      ]
    ];
    return $form;
  }


  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $fids = $form_state->getValue('carousel_images');
    $this->setConfigurationValue('carousel_images', $fids);
    $ui = $form_state->getUserInput();
    $carousel_images_config = [];
    foreach($fids as $fid) {
      $file = File::load($fid);
      $file->setPermanent();
      $file->save();
      $key = 'file_' . $fid;
      $carousel_images_config[$fid] = [
        'header' => Html::escape($ui['settings']['carousel_images']['carousel_images_order'][$key]['header']),
        'description' => Html::escape($ui['settings']['carousel_images']['carousel_images_order'][$key]['description']),
        'link' => Html::escape($ui['settings']['carousel_images']['carousel_images_order'][$key]['link']),
        'target' => !empty($ui['settings']['carousel_images']['carousel_images_order'][$key]['target']),
        'weight' => $ui['settings']['carousel_images']['carousel_images_order'][$key]['weight'],
      ];
    }
    $this->setConfigurationValue('carousel_images_config', $carousel_images_config);
    $this->setConfigurationValue('style', $form_state->getValue('style'));
    $this->setConfigurationValue('suffix', $form_state->getValue('suffix'));
    $this->setConfigurationValue('prefix', $form_state->getValue('prefix'));
    $this->setConfigurationValue('width', $form_state->getValue('width'));
    $this->setConfigurationValue('height', $form_state->getValue('height'));
    $this->setConfigurationValue('autoplay', $form_state->getValue('autoplay'));
    $this->setConfigurationValue('autoplay_interval', $form_state->getValue('autoplay_interval'));
  }

  public function build() {
    $div_id = Html::getUniqueId('bcarousel');
    $config = $this->getConfiguration();
    $return['#cache'] = array(
      'contexts' => ['url'],
      'max-age' => 0,
    );
    $images = $config['carousel_images'] ?? [];
    if (empty($images)) {
      return $return;
    }
    $carousel_images_config = $config['carousel_images_config'] ?? [];
    uasort($images, function($a, $b) use ($carousel_images_config) {
      $a = $carousel_images_config[$a]['weight'] ?? 0;
      $b = $carousel_images_config[$b]['weight'] ?? 0;
      return $a > $b;
    });
    $items = [];
    $style_name = $config['style'] ?? '';
    foreach($images as $fid) {
      $image = File::load($fid);
      if (!$image ) {
        continue;
      }
      if ($style_name) {
        $style = ImageStyle::load($style_name);
        $image_url = $style->buildUrl($image->getFileUri());
      }
      else {
        $image_url = $image->createFileUrl(false);
      }
      $width = $config['width'] ?? 200;
      $height = $config['height'] ?? 400;

      // TODO - refactor this nicer with renderable arrays
      $link = $carousel_images_config[$fid]['link'] ?? null;
      $target = $carousel_images_config[$fid]['target'] ?? null;
      $header = $carousel_images_config[$fid]['header'] ?? null;
      $description = $carousel_images_config[$fid]['description'] ?? null;
      $image_link = sprintf('<img src="%s" width="%d" height="%d" />', $image_url, $width, $height);
      $markup = $image_link;
      if ($header) {
        $markup .= '<h2 class="boutique-frame-title">' . $header . '</h2>';
      }
      if ($description) {
        $markup .= '<div class="boutique-frame-text">' . $description . '</div>';
      }
      if ($link) {
        if ($target) {
          $markup = '<a href="' . $link . '" target="_blank">' . $markup . '</a>';
        }
        else {
          $markup = '<a href="' . $link . '">' . $markup . '</a>';
        }
      }
      $items[$fid] = [
        '#markup' => $markup,
      ];
    }
    if (!empty($config['prefix']['value'])) {
      $return['prefix'] = [
        '#type' => 'processed_text',
        '#text' => $config['prefix']['value'],
        '#format' => $config['prefix']['format'],
      ];
    }
    $return['carousel'] = [
      '#theme' => 'item_list',
      '#items' => $items,
      '#attributes' => ['id' => $div_id, 'class' => ['boutique-carousel']]
    ];
    if (!empty($config['suffix']['value'])) {
      $return['suffix'] = [
        '#type' => 'processed_text',
        '#text' => $config['suffix']['value'],
        '#format' => $config['suffix']['format'],
      ];
    }
    $return['#attached']['library'][] = 'boutique_carousel/core';
    $return['#attached']['drupalSettings']['boutique_carousel'][$div_id]['autoplay'] = $config['autoplay'] ?? 0;
    $return['#attached']['drupalSettings']['boutique_carousel'][$div_id]['autoplay_interval'] = $config['autoplay_interval'] ?? 3000;
    return $return;
  }
}

<?php

namespace Drupal\boutique_carousel\Element;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\Element\ManagedFile;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides an AJAX/progress aware widget for uploading and saving a file.
 *
 * @FormElement("boutique_managed_file")
 */
class BoutiqueManagedFileFormElement extends ManagedFile {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $ret = parent::getInfo();
    $ret['#display_filename_input'] = FALSE;
    return $ret;
  }

  public static function processManagedFile(&$element, FormStateInterface $form_state, &$complete_form) {
    $element = parent::processManagedFile($element, $form_state, $complete_form);
    $fids = isset($element['#value']['fids']) ? $element['#value']['fids'] : [];
    // Generate a unique wrapper HTML ID.
    preg_match('/<div id="(.*)">/', $element['#prefix'], $matches);
    $element['#attached']['library'][] = 'boutique_carousel/admin';
    if (!empty($fids) && $element['#files']) {
      $group_class = 'group-order-weight';
      $element['select_all'] = [
        '#type' => 'checkbox',
        '#title' => new TranslatableMarkup('Select all'),
        '#attributes' => ['class' => ['select-all']],
        '#weight' => 0,
      ];
      $element['remove_button']['#weight'] = -1;
      $element['carousel_images_order'] = [
        '#type' => 'table',
        '#weight' => 50,
        '#header' => [
          [
            'data' => new TranslatableMarkup('Order'),
            'class' => 'col-order'
          ],
          [
            'data' => new TranslatableMarkup('Image'),
            'class' => 'col-image'
          ],
          [
            'data' => new TranslatableMarkup('Header'),
            'class' => 'col-header'
          ],
          [
            'data' => new TranslatableMarkup('Description'),
            'class' => 'col-description'
          ],
          [
            'data' => new TranslatableMarkup('Link'),
            'class' => 'col-link'
          ],
          [
            'data' => new TranslatableMarkup('Open in new tab'),
            'class' => 'col-tab'
          ],
          new TranslatableMarkup('Weight'),
        ],
        '#tableselect' => false,
        '#caption' => new TranslatableMarkup('Reorder images'),
        '#tabledrag' => [[
          'action' => 'order',
          'relationship' => 'sibling',
          /* hides the WEIGHT & PARENT tree columns below */
          'hidden' => TRUE,
          'group' => $group_class,
        ]],
      ];

      $carousel_images_config = $element['#carousel_images_config'] ?? [];
      uasort($element['#files'], function ($a, $b) use ($carousel_images_config) {
        $wa = $carousel_images_config[$a->id()] ?? 0;
        $wb = $carousel_images_config[$b->id()] ?? 0;
        if ($wa['weight'] == $wb['weight']) {
          return 0;
        }
        return ($wa['weight'] < $wb['weight']) ? -1 : 1;
      });
      /**
       * @var int $delta
       * @var \Drupal\file\Entity\File $file
       */
      foreach ($element['#files'] as $delta => $file) {
        $key = 'file_' . $delta;
        $weight = $carousel_images_config[$file->id()]['weight'] ?? 0;
        $element[$key]['selected']['#attributes']['class'][] = 'select-file-for-removal';
        if ($element['#multiple']) {
          $element['carousel_images_order'][$key]['#attributes']['class'][] = 'draggable';
          $element['carousel_images_order'][$key]['#weight'] = $weight;
          $element['carousel_images_order'][$key]['fake-col'] = ['#markup' => ''];
          $element['carousel_images_order'][$key]['image'] = [
            '#markup' => Markup::create('<img src="' . $file->url() . '" width="100" height="100"/>'),
          ];
          $element['carousel_images_order'][$key]['header'] = [
            '#type' => 'textfield',
            '#default_value' => $carousel_images_config[$file->id()]['header'] ?? '',
            '#wrapper_attributes' => ['class' => ['input-header']],
          ];
          $element['carousel_images_order'][$key]['description'] = [
            '#type' => 'textfield',
            '#default_value' => $carousel_images_config[$file->id()]['description'] ?? '',
            '#wrapper_attributes' => ['class' => ['input-description']],
          ];
          $element['carousel_images_order'][$key]['link'] = [
            '#type' => 'textfield',
            '#default_value' => $carousel_images_config[$file->id()]['link'] ?? '',
            '#wrapper_attributes' => ['class' => ['input-link']],
          ];
          $element['carousel_images_order'][$key]['target'] = [
            '#type' => 'checkbox',
            '#title' => new TranslatableMarkup('Yes'),
            '#default_value' => $carousel_images_config[$file->id()]['target'] ?? '',
            '#wrapper_attributes' => ['class' => ['input-tab']],
          ];
          $element['carousel_images_order'][$key]['weight'] = [
            '#type' => 'weight',
            '#title' => new TranslatableMarkup('Weight for @title', ['@title' => $file->getFilename()]),
            '#title_display' => 'invisible',
            '#default_value' => $weight,
            '#delta' => 100,
            '#attributes' => ['class' => [$group_class]],
          ];
        }
      }
    }
    return $element;
  }
}

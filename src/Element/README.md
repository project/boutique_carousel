CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

This module allows creating blocks presenting a carousel of images. Each block
can have its own images and each image can have a description, heading and
link that opens in same tab or new tab.

INSTALLATION
------------

Install the Field Widget module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.

REQUIREMENTS
-------------

* This module requires you to buy a license for the Boutique Carousel jQuery
plugin from here: https://frique.me/_demos/boutique/?v=154, it's only $9.

CONFIGURATION
-------------

* This module requires no configuration.
* Recommend to clear Drupal cache.

MAINTAINERS
-----------

Current maintainers:
 * Cristian Romanescu - https://www.drupal.org/u/cristiroma

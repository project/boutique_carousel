(function ($, Drupal) {
  Drupal.behaviors.boutique = {
    attach: function (context, settings) {
      $('.boutique-carousel').once('boutique_run').each(function(){
        var id = $(this).attr('id');
        var settings = drupalSettings.boutique_carousel[id];
        var autoplay_interval = 3000;
        if(typeof settings['autoplay_interval'] != 'undefined' && settings['autoplay_interval'] > 0) {
          autoplay_interval = settings['autoplay_interval'];
        }
        var autoplay_method = id  + '_startautoplay';
        $(this).boutique({'autoplay_interval': autoplay_interval});
        if (typeof settings.autoplay === 'number' && settings.autoplay == 1 && typeof window[autoplay_method] === 'function') {
          var fn = window[autoplay_method];
          fn();
        }
      });
    }
  };
})(jQuery, Drupal);

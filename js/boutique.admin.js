/**
 * @file
 * Provides JavaScript additions to the managed file field type.
 *
 * This file provides 'Select all' checkbox functionality.
 */

(function ($, Drupal) {

  /**
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches triggers for the 'Select all' checkbox.
   */
  Drupal.behaviors.bulk_select_all = {
    attach: function (context) {
      $(context).find('input[type="checkbox"].select-all').once('bc-select-all').on('change', function() {
        var checked = this.checked;
        $('input[type="checkbox"].select-file-for-removal', $(context)).each(function() {
          this.checked = checked;
        });
      });
    }
  };
})(jQuery, Drupal);
